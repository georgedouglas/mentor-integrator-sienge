FROM openjdk:8-jre
RUN mkdir app
ARG JAR_FILE
ADD /target/${JAR_FILE} /app/mentor.jar
WORKDIR /app
ENTRYPOINT bash -c "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar mentor.jar"
