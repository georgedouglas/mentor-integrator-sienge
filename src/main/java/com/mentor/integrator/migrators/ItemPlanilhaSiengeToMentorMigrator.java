package com.mentor.integrator.migrators;

import com.mentor.integrator.mentor.composicao_servico_informado.ComposicaoServicoInformadoMentor;
import com.mentor.integrator.mentor.grupo_insumo.GrupoInsumoMentor;
import com.mentor.integrator.mentor.grupo_insumo.GrupoInsumoMentorService;
import com.mentor.integrator.mentor.item_planilha_orcamento.ItemPlanilhaOrcamentoMentor;
import com.mentor.integrator.mentor.item_planilha_orcamento.ItemPlanilhaOrcamentoMentorService;
import com.mentor.integrator.mentor.item_planilha_orcamento.NivelItemPlanilha;
import com.mentor.integrator.mentor.obra.ObraMentor;
import com.mentor.integrator.mentor.orcamento.OrcamentoMentor;
import com.mentor.integrator.mentor.orcamento.OrcamentoMentorService;
import com.mentor.integrator.mentor.servico_obra.ServicoObraMentor;
import com.mentor.integrator.mentor.servico_obra.ServicoObraMentorService;
import com.mentor.integrator.mentor.unidade_medida.UnidadeMedidaMentor;
import com.mentor.integrator.mentor.unidade_medida.UnidadeMedidaMentorService;
import com.mentor.integrator.sienge.item_planilha.ItemPlanilhaSienge;
import com.mentor.integrator.sienge.item_planilha.ItemPlanilhaSiengeWebClient;
import com.mentor.integrator.sienge.planilha.PlanilhaSienge;
import com.mentor.integrator.sienge.planilha.PlanilhaSiengeWebClient;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ItemPlanilhaSiengeToMentorMigrator {

    private Logger logger = LoggerFactory.getLogger(ItemPlanilhaSiengeToMentorMigrator.class);

    private final OrcamentoMentorService orcamentoMentorService;
    private final PlanilhaSiengeWebClient planilhaSiengeWebClient;
    private final ItemPlanilhaSiengeWebClient itemPlanilhaSiengeWebClient;
    private final ItemPlanilhaOrcamentoMentorService itemPlanilhaOrcamentoMentorService;
    private final UnidadeMedidaMentorService unidadeMedidaMentorService;
    private final ServicoObraMentorService servicoObraMentorService;
    private final GrupoInsumoMentorService grupoInsumoMentorService;

    @Transactional
    public void migrar(){

        List<ItemPlanilhaOrcamentoMentor> itemPlanilhaOrcamentoCriadosLog = new ArrayList<>();
        List<UnidadeMedidaMentor> unidadeMedidaCriadasLog = new ArrayList<>();
        List<ServicoObraMentor> servicoObraCriadosLog = new ArrayList<>();
        List<GrupoInsumoMentor> grupoInsumoCriadosLog = new ArrayList<>();
        List<ComposicaoServicoInformadoMentor> composicaoServicoInformadoCriadosLog = new ArrayList<>();

        try{

            List<ObraMentor> obrasMentor = orcamentoMentorService.buscarTodasObrasSienge();

            obrasMentor.forEach( obraMentor -> {
                List<PlanilhaSienge> planilhasSienge = planilhaSiengeWebClient.buscarTodos(obraMentor.getIdObraIntegracao());
                if(planilhasSienge != null){
                    for(PlanilhaSienge planilhaSienge : planilhasSienge){

                        List<ItemPlanilhaSienge> itensPlanilha = itemPlanilhaSiengeWebClient
                                .buscarTodos(obraMentor.getIdObraIntegracao(), planilhaSienge.getId());

                        OrcamentoMentor orcamentoMentor = orcamentoMentorService.buscarOrcamentoPorObraCodigo(planilhaSienge, obraMentor);

                        itensPlanilha.forEach( itemPlanilha -> {

                            if(itemPlanilhaOrcamentoMentorService.deveMigrarItemPlanilha(itemPlanilha, orcamentoMentor)){

                                NivelItemPlanilha nivel = NivelItemPlanilha.getNivel(itemPlanilha.getWbsCode());

                                //migração de item planilha orcamento
                                ItemPlanilhaOrcamentoMentor itemPlanilhaOrcamentoMentor = itemPlanilhaOrcamentoMentorService
                                        .migrarItemPlanilha(itemPlanilha, nivel, orcamentoMentor);
                                itemPlanilhaOrcamentoCriadosLog.add(itemPlanilhaOrcamentoMentor);

                                if(nivel.equals(NivelItemPlanilha.SERVICO)){

                                    //migração de unidade medida
                                    UnidadeMedidaMentor unidadeMedida = unidadeMedidaMentorService.buscarUnidadeMedida(itemPlanilha.getUnitOfMeasure());
                                    if(unidadeMedida == null) {
                                        unidadeMedida = unidadeMedidaMentorService.migrarUnidadeMedida(itemPlanilha.getUnitOfMeasure());
                                        unidadeMedidaCriadasLog.add(unidadeMedida);
                                    }

                                    //migração de servico obra
                                    ServicoObraMentor servicoObraMentor = servicoObraMentorService
                                            .migrarServicoObra(itemPlanilha, unidadeMedida, itemPlanilhaOrcamentoMentor);
                                    servicoObraCriadosLog.add(servicoObraMentor);

                                    itemPlanilha.getPricesByCategory().forEach(priceByCategorySienge -> {
                                        GrupoInsumoMentor grupoInsumoMentor = grupoInsumoMentorService.buscar(priceByCategorySienge);
                                        if(grupoInsumoMentor == null){
                                            //migração de grupo insumo
                                            grupoInsumoMentor = grupoInsumoMentorService.migrar(priceByCategorySienge);
                                            grupoInsumoCriadosLog.add(grupoInsumoMentor);
                                        }

                                        //migração de composição servico informado
                                        ComposicaoServicoInformadoMentor composicaoCriado = servicoObraMentorService
                                                .migrarComposicaoServicoInformado(servicoObraMentor, priceByCategorySienge, grupoInsumoMentor);
                                        composicaoServicoInformadoCriadosLog.add(composicaoCriado);
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }catch (Exception ex){
            logger.error("Error=["+ex.getMessage()+"]");
            throw ex;
        }

        if(teveAtualizacao(itemPlanilhaOrcamentoCriadosLog, unidadeMedidaCriadasLog, servicoObraCriadosLog, grupoInsumoCriadosLog, composicaoServicoInformadoCriadosLog)){

            logger.info("\n======================= INICIO Importação Item Planilha Sienge =======================\n" +
                    "Foram criadas ["+itemPlanilhaOrcamentoCriadosLog.size()+"] itens planilhas orcamento. Detalhes: \n["+ ItemPlanilhaOrcamentoMentor.toLog(itemPlanilhaOrcamentoCriadosLog)+"] \n" +
                    "Foram criadas ["+unidadeMedidaCriadasLog.size()+"] unidades de medidas. Detalhes: \n["+UnidadeMedidaMentor.toLog(unidadeMedidaCriadasLog)+"] \n" +
                    "Foram criados ["+servicoObraCriadosLog.size()+"] servicos obras. Detalhes: \n["+ServicoObraMentor.toLog(servicoObraCriadosLog)+"] \n" +
                    "Foram criados ["+grupoInsumoCriadosLog.size()+"] grupos de insumos. Detalhes: \n["+GrupoInsumoMentor.toLog(grupoInsumoCriadosLog)+"] \n" +
                    "Foram criados ["+composicaoServicoInformadoCriadosLog.size()+"] composicao servico informado. Detalhes: \n["+ComposicaoServicoInformadoMentor.toLog(composicaoServicoInformadoCriadosLog)+"] \n" +
                    "======================= FIM Importação Item Planilha Sienge =======================\n");
        }
    }

    static <T> boolean teveAtualizacao(List<? extends T>... listaAtualizacoes){
        for (List<? extends T> list : listaAtualizacoes) {
            if(!list.isEmpty()) return true;
        }
        return false;
    }
}
