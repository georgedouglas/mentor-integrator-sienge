package com.mentor.integrator.migrators;

import com.mentor.integrator.mentor.obra.ObraMentor;
import com.mentor.integrator.mentor.orcamento.OrcamentoMentor;
import com.mentor.integrator.mentor.orcamento.OrcamentoMentorService;
import com.mentor.integrator.mentor.orcamento.VersaoOrcamentoMentor;
import com.mentor.integrator.sienge.planilha.PlanilhaSienge;
import com.mentor.integrator.sienge.planilha.PlanilhaSiengeWebClient;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PlanilhaSiengeToMentorMigrator {

    private Logger logger = LoggerFactory.getLogger(PlanilhaSiengeToMentorMigrator.class);

    private final OrcamentoMentorService orcamentoMentorService;
    private final PlanilhaSiengeWebClient planilhaSiengeWebClient;

    @Transactional
    public void migrar(){

        List<VersaoOrcamentoMentor> versoesOrcamentoCriadosLog = new ArrayList<>();
        List<OrcamentoMentor> orcamentosAtualizadosLog = new ArrayList<>();
        List<OrcamentoMentor> orcamentosCriadosLog= new ArrayList<>();

        try {
            List<ObraMentor> obrasMentor = orcamentoMentorService.buscarTodasObrasSienge();

            obrasMentor.forEach( obraMentor -> {
                List<PlanilhaSienge> planilhas = planilhaSiengeWebClient.buscarTodos(obraMentor.getIdObraIntegracao());
                if(planilhas != null && !planilhas.isEmpty()){

                    VersaoOrcamentoMentor versaoOrcamento = orcamentoMentorService.recuperarVersaoOrcamento(obraMentor.getId());

                    if(versaoOrcamento == null){
                        //migração de versao orcamento
                        versaoOrcamento = orcamentoMentorService.migrarVersaoOrcamento(obraMentor);
                        versoesOrcamentoCriadosLog.add(versaoOrcamento);

                        if(orcamentoMentorService.temOrcamento(obraMentor.getId())){
                            //atualização de orcamento - versao orcamento
                            List<OrcamentoMentor> orcamentosAtualizados = orcamentoMentorService.atualizarVersaoOrcamento(obraMentor, versaoOrcamento);
                            orcamentosAtualizadosLog.addAll(orcamentosAtualizados);
                        }
                    }

                    for(PlanilhaSienge planilha : planilhas){
                        if(orcamentoMentorService.deveMigrarOrcamento(planilha, obraMentor)){
                            //migração de orcamento
                            OrcamentoMentor orcamentoCriado = orcamentoMentorService.migrarOrcamento(planilha, obraMentor, versaoOrcamento);
                            orcamentosCriadosLog.add(orcamentoCriado);
                        }
                    }
                }
            });

        }catch (Exception ex){
            logger.error("Error=["+ex.getMessage()+"]");
            throw ex;
        }

        if(teveAtualizacao(versoesOrcamentoCriadosLog, orcamentosAtualizadosLog, orcamentosCriadosLog)){
            logger.info("\n======================= INICIO Importação Planilha Sienge =======================\n" +
                    "Foram criadas ["+versoesOrcamentoCriadosLog.size()+"] versoes de orcamento. Detalhes: \n["+VersaoOrcamentoMentor.toLog(versoesOrcamentoCriadosLog)+"] \n" +
                    "Foram atualizados ["+orcamentosAtualizadosLog.size()+"] orcamentos com nova versao. Detalhes: \n["+OrcamentoMentor.toLog(orcamentosAtualizadosLog)+"] \n" +
                    "Foram criados ["+orcamentosCriadosLog.size()+"] orcamentos. Detalhes: \n["+OrcamentoMentor.toLog(orcamentosCriadosLog)+"] \n" +
                    "======================= FIM Importação Planilha Sienge =======================\n");
        }
    }

    static <T> boolean teveAtualizacao(List<? extends T>... listaAtualizacoes){
        for (List<? extends T> list : listaAtualizacoes) {
            if(!list.isEmpty()) return true;
        }
        return false;
    }
}

