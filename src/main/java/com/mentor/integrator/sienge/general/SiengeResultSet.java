package com.mentor.integrator.sienge.general;

import lombok.*;

import java.util.List;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class SiengeResultSet<T> {

    private SiengeResultSetMetadata resultSetMetadata;
    private List<T> results;

}
