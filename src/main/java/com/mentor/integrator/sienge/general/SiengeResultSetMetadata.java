package com.mentor.integrator.sienge.general;

import lombok.*;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@RequiredArgsConstructor
public class SiengeResultSetMetadata {

    private int count;
    private int offset;
    private int limit;
}
