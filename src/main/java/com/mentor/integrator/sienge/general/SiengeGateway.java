package com.mentor.integrator.sienge.general;


import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class SiengeGateway {

    public static final int LIMIT = 100;

    public String buildEndpoint(String... pathsValues){
        return String.join("/", pathsValues);
    }
}
