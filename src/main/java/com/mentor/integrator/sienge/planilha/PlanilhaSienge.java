package com.mentor.integrator.sienge.planilha;

import lombok.*;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@RequiredArgsConstructor
public class PlanilhaSienge {

    private Long id;
    private String description;
}
