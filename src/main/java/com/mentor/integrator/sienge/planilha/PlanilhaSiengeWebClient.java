package com.mentor.integrator.sienge.planilha;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mentor.integrator.sienge.general.SiengeGateway;
import com.mentor.integrator.sienge.general.SiengeResultSet;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class PlanilhaSiengeWebClient extends SiengeGateway {

    private Logger logger = LoggerFactory.getLogger(PlanilhaSiengeWebClient.class);


    private final WebClient webClient;
    private static final String FIRST_PATH = "building-cost-estimations";
    private static final String FIRST_VALUE_NAME = "{buildingId}";
    private static final String SECOND_PATH = "sheets";

    public List<PlanilhaSienge> buscarTodos(Long buildingId){

        int count = 0;
        int offset = 0;
        List<PlanilhaSienge> planilhas = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();

        while (offset <= count){
            try {
                SiengeResultSet response = get(buildingId, offset);
                offset += LIMIT;

                count = response.getResultSetMetadata().getCount();
                planilhas.addAll(mapper.convertValue(response.getResults(), new TypeReference<List<PlanilhaSienge>>() { }));
            } catch (RuntimeException ex){
                logger.error("Error while calling endpoint "+
                        buildEndpoint(FIRST_PATH, FIRST_VALUE_NAME, SECOND_PATH)+". Error=["+ex.getMessage()+"]");
                throw ex;
            }
        }

        return planilhas;
    }

    private SiengeResultSet get(Long buildingId, int offset){
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .pathSegment(FIRST_PATH, FIRST_VALUE_NAME, SECOND_PATH)
                        .queryParam("limit", LIMIT)
                        .queryParam("offset", offset)
                        .build(buildingId))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(HttpStatus::isError, clientResponse -> {
                    throw new RuntimeException(""+clientResponse.statusCode());
                })
                .toEntity(SiengeResultSet.class)
                .block().getBody();
    }
}
