package com.mentor.integrator.sienge.item_planilha;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.math.BigDecimal;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceByCategorySienge {

    private String category;
    private BigDecimal unitPrice;
}
