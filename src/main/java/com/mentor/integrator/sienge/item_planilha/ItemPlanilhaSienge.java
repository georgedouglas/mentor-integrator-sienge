package com.mentor.integrator.sienge.item_planilha;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemPlanilhaSienge {

    private Long id;
    private String wbsCode;
    private String description;
    private String unitOfMeasure;
    private BigDecimal quantity;
    private List<PriceByCategorySienge> pricesByCategory;
}
