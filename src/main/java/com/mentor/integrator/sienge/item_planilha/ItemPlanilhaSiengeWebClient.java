package com.mentor.integrator.sienge.item_planilha;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mentor.integrator.sienge.general.SiengeGateway;
import com.mentor.integrator.sienge.general.SiengeResultSet;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ItemPlanilhaSiengeWebClient extends SiengeGateway {

    private Logger logger = LoggerFactory.getLogger(ItemPlanilhaSiengeWebClient.class);

    private final WebClient webClient;
    private static final String FIRST_PATH = "building-cost-estimations";
    private static final String FIRST_VALUE_NAME = "{buildingId}";
    private static final String SECOND_PATH = "sheets";
    private static final String SECOND_VALUE_NAME = "{buildingUnitId}";
    private static final String THIRD_PATH = "items";

    public List<ItemPlanilhaSienge> buscarTodos(Long buildingId, Long buildingUnitId){

        int count = 0;
        int offset = 0;
        List<ItemPlanilhaSienge> itemPlanilhas = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();

        while (offset <= count){
            try {
                SiengeResultSet response = get(buildingId, buildingUnitId, offset);
                offset += LIMIT;

                count = response.getResultSetMetadata().getCount();
                itemPlanilhas.addAll(mapper.convertValue(response.getResults(), new TypeReference<List<ItemPlanilhaSienge>>() { }));
            } catch (RuntimeException ex){
                logger.error("Error while calling endpoint "+
                        buildEndpoint(FIRST_PATH, FIRST_VALUE_NAME, SECOND_PATH, SECOND_VALUE_NAME, THIRD_PATH)+
                        ". Error=["+ex.getMessage()+"]");
                throw ex;
            }
        }

        return itemPlanilhas;
    }

    private SiengeResultSet get(Long buildingId, Long buildingUnitId, int offset){
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .pathSegment(FIRST_PATH, FIRST_VALUE_NAME, SECOND_PATH, SECOND_VALUE_NAME, THIRD_PATH)
                        .queryParam("limit", LIMIT)
                        .queryParam("offset", offset)
                        .build(buildingId, buildingUnitId))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(HttpStatus::isError, clientResponse -> {
                    throw new RuntimeException(""+clientResponse.statusCode());
                })
                .toEntity(SiengeResultSet.class)
                .block().getBody();
    }
}
