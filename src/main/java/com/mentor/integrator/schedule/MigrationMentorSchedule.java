package com.mentor.integrator.schedule;

import com.mentor.integrator.migrators.ItemPlanilhaSiengeToMentorMigrator;
import com.mentor.integrator.migrators.PlanilhaSiengeToMentorMigrator;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MigrationMentorSchedule {

    private final PlanilhaSiengeToMentorMigrator planilhaSiengeToMentorMigrator;
    private final ItemPlanilhaSiengeToMentorMigrator itemPlanilhaSiengeToMentorMigrator;

    @Scheduled(fixedRateString = "${time.schedule}", initialDelay = 1000)
    public void checkUpdate(){
        planilhaSiengeToMentorMigrator.migrar();
        itemPlanilhaSiengeToMentorMigrator.migrar();
    }
}
