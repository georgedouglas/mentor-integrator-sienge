package com.mentor.integrator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MentorIntegratorSiengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MentorIntegratorSiengeApplication.class, args);
	}

}
