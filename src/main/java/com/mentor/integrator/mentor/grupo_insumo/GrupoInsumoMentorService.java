package com.mentor.integrator.mentor.grupo_insumo;

import com.mentor.integrator.sienge.item_planilha.PriceByCategorySienge;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;

@Service
@RequiredArgsConstructor
public class GrupoInsumoMentorService {

    private final GrupoInsumoMentorRepository grupoInsumoMentorRepository;


    public GrupoInsumoMentor buscar(PriceByCategorySienge priceByCategorySienge){
        return grupoInsumoMentorRepository
                .buscarPorDescricao(priceByCategorySienge.getCategory())
                .stream().max(Comparator.comparing(GrupoInsumoMentor::getId)).orElse(null);
    }

    public GrupoInsumoMentor migrar(PriceByCategorySienge priceByCategorySienge) {
        GrupoInsumoMentor grupoInsumo = GrupoInsumoMentor.builder()
                .descricao(priceByCategorySienge.getCategory())
                .build();

        return grupoInsumoMentorRepository.save(grupoInsumo);
    }
}
