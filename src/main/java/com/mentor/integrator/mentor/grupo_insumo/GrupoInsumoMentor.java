package com.mentor.integrator.mentor.grupo_insumo;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "grupo_insumo")
public class GrupoInsumoMentor {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_grupo_insumo")
    private Long id;

    @Column(name = "descricao")
    private String descricao;

    public static String toLog(List<GrupoInsumoMentor> gruposInsumo){
        return gruposInsumo.stream().map(grupoInsumo -> "descricao="+grupoInsumo.getDescricao()+";\n").collect(Collectors.joining());
    }
}
