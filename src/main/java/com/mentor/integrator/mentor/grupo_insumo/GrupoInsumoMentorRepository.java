package com.mentor.integrator.mentor.grupo_insumo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GrupoInsumoMentorRepository extends CrudRepository<GrupoInsumoMentor, Long> {

    @Query(value = "SELECT g FROM GrupoInsumoMentor g WHERE g.descricao = :descricao")
    List<GrupoInsumoMentor> buscarPorDescricao(@Param("descricao") String descricao);
}
