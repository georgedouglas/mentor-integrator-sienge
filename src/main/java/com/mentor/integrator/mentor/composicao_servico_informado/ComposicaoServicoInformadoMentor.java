package com.mentor.integrator.mentor.composicao_servico_informado;

import com.mentor.integrator.mentor.grupo_insumo.GrupoInsumoMentor;
import com.mentor.integrator.mentor.servico_obra.ServicoObraMentor;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "composicao_servico_informado")
public class ComposicaoServicoInformadoMentor {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_composicao_servico_informado")
    private Long id;

    @Column(name = "data")
    private OffsetDateTime data;

    @Column(name = "preco_unitario")
    private BigDecimal precoUnitario;

    @ManyToOne
    @JoinColumn(name="id_grupo_insumo")
    private GrupoInsumoMentor grupoInsumoMentor;

    @ManyToOne
    @JoinColumn(name="id_servico_obra")
    private ServicoObraMentor servicoObra;

    public static String toLog(List<ComposicaoServicoInformadoMentor> composicoes){
        return composicoes.stream().map(composicao -> "idServicoObra="+composicao.getServicoObra().getId()+";\n").collect(Collectors.joining());
    }
}
