package com.mentor.integrator.mentor.composicao_servico_informado;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComposicaoServicoInformadoMentorRepository extends CrudRepository<ComposicaoServicoInformadoMentor, Long> {
}
