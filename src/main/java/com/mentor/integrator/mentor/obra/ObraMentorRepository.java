package com.mentor.integrator.mentor.obra;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class ObraMentorRepository {

    private final JdbcTemplate jdbcTemplateMentor;  // jdbc para consultas normais
    private final NamedParameterJdbcTemplate namedJdbcTemplateMentor; // jdbc que facilita a utilizacao de parametros e transacoes em lote

    public List<ObraMentor> buscarTodasObrasSienge() {
        return jdbcTemplateMentor.query(
                ObraMentorQueries.CONSULTA_TODAS_OBRAS_SIENGE,
                (resultSet, rowNum) -> ObraMentor.montar(resultSet)
        );
    }
}
