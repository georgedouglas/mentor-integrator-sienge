package com.mentor.integrator.mentor.obra;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.sql.ResultSet;
import java.sql.SQLException;

@Builder
@ToString
@EqualsAndHashCode
@Getter
public class ObraMentor {

    private final Long id;
    private final Long idObraIntegracao;

    public static ObraMentor montar(final ResultSet rs) throws SQLException {

        return ObraMentor.builder()
                .id(rs.getLong("id_obra"))
                .idObraIntegracao(rs.getLong("id_obra_integracao"))
                .build();
    }
}
