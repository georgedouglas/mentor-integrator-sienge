package com.mentor.integrator.mentor.servico_obra;

import com.mentor.integrator.mentor.composicao_servico_informado.ComposicaoServicoInformadoMentor;
import com.mentor.integrator.mentor.composicao_servico_informado.ComposicaoServicoInformadoMentorRepository;
import com.mentor.integrator.mentor.grupo_insumo.GrupoInsumoMentor;
import com.mentor.integrator.mentor.item_planilha_orcamento.ItemPlanilhaOrcamentoMentor;
import com.mentor.integrator.mentor.unidade_medida.UnidadeMedidaMentor;
import com.mentor.integrator.sienge.item_planilha.ItemPlanilhaSienge;
import com.mentor.integrator.sienge.item_planilha.PriceByCategorySienge;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
@RequiredArgsConstructor
public class ServicoObraMentorService {

    private final ServicoObraMentorRepository servicoObraMentorRepository;
    private final ComposicaoServicoInformadoMentorRepository composicaoServicoInformadoMentorRepository;

    public ServicoObraMentor migrarServicoObra(
            ItemPlanilhaSienge itemPlanilha,
            UnidadeMedidaMentor unidadeMedida,
            ItemPlanilhaOrcamentoMentor itemPlanilhaOrcamentoMentor
        ){
        ServicoObraMentor servicoObraMentor = ServicoObraMentor.builder()
                .descricao(itemPlanilha.getDescription())
                .idUnidadeMedida(unidadeMedida.getId())
                .quantidade(itemPlanilha.getQuantity())
                .idItemPlanilhaOrcamento(itemPlanilhaOrcamentoMentor.getId())
                .build();

        return servicoObraMentorRepository.save(servicoObraMentor);
    }

    public ComposicaoServicoInformadoMentor migrarComposicaoServicoInformado(
            ServicoObraMentor servicoObra,
            PriceByCategorySienge priceByCategorySienge,
            GrupoInsumoMentor grupoInsumoMentor) {

        ComposicaoServicoInformadoMentor composicaoServicoInformado = ComposicaoServicoInformadoMentor.builder()
                .data(OffsetDateTime.now())
                .precoUnitario(priceByCategorySienge.getUnitPrice())
                .grupoInsumoMentor(grupoInsumoMentor)
                .servicoObra(servicoObra).build();

        return composicaoServicoInformadoMentorRepository.save(composicaoServicoInformado);
    }
}
