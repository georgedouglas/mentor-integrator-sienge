package com.mentor.integrator.mentor.servico_obra;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServicoObraMentorRepository extends CrudRepository<ServicoObraMentor, Long> {

}
