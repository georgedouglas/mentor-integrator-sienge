package com.mentor.integrator.mentor.servico_obra;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "servico_obra")
public class ServicoObraMentor {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_servico_obra")
    private Long id;

    @Column(name = "id_unidade_medida")
    private Long idUnidadeMedida;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "quantidade")
    private BigDecimal quantidade;

    @Column(name = "tipo_preco_servico")
    private int tipoPreco = 0;

    @Column(name="id_item_planilha_orcamento", nullable = false)
    private Long idItemPlanilhaOrcamento;

    public static String toLog(List<ServicoObraMentor> servicosObra){
        return servicosObra.stream().map(servicoObra -> "descricao="+servicoObra.getDescricao()+";idItemPlanilhaOrcamento="+servicoObra.getIdItemPlanilhaOrcamento()+";\n").collect(Collectors.joining());
    }
}
