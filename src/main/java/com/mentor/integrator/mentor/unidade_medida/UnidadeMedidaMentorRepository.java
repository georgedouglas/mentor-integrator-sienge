package com.mentor.integrator.mentor.unidade_medida;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UnidadeMedidaMentorRepository extends CrudRepository<UnidadeMedidaMentor, Long> {

    @Query(value = "SELECT u FROM UnidadeMedidaMentor u WHERE u.sigla = :unidadeMedida")
    UnidadeMedidaMentor buscarUnidadeMedidaPorSigla(@Param("unidadeMedida") String unidadeMedida);
}
