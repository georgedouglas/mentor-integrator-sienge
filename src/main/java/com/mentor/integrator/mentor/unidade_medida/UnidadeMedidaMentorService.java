package com.mentor.integrator.mentor.unidade_medida;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UnidadeMedidaMentorService {

    private final UnidadeMedidaMentorRepository unidadeMedidaMentorRepository;

    public UnidadeMedidaMentor buscarUnidadeMedida(String unidadeMedida){
        if(unidadeMedida == null) return null;
        return unidadeMedidaMentorRepository.buscarUnidadeMedidaPorSigla(unidadeMedida);
    }

    public UnidadeMedidaMentor migrarUnidadeMedida(String unidadeMedida) {

        UnidadeMedidaMentor novaUnidadeMedida = UnidadeMedidaMentor.builder()
                .descricao(unidadeMedida)
                .sigla(unidadeMedida).build();
        return unidadeMedidaMentorRepository.save(novaUnidadeMedida);
    }
}
