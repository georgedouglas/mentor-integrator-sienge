package com.mentor.integrator.mentor.unidade_medida;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "unidade_medida")
public class UnidadeMedidaMentor {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_unidade_medida")
    private Long id;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "sigla")
    private String sigla;

    public static String toLog(List<UnidadeMedidaMentor> unidadesMedidas){
        return unidadesMedidas.stream().map(unidadeMedida -> "sigla="+unidadeMedida.getSigla()+";\n").collect(Collectors.joining());
    }
}
