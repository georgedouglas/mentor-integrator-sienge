package com.mentor.integrator.mentor.orcamento;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VersaoOrcamentoMentorRepository extends CrudRepository<VersaoOrcamentoMentor, Long> {

    @Query(value = "SELECT v FROM VersaoOrcamentoMentor v WHERE v.versaoAtual = true and v.idObra = :idObra")
    List<VersaoOrcamentoMentor> buscarVersaoOrcamentoPorObra(@Param("idObra") Long idObra);
}
