package com.mentor.integrator.mentor.orcamento;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orcamento")
public class OrcamentoMentor {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_orcamento")
    private Long id;

    @Column(name = "codigo")
    private Long codigo;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "id_obra")
    private Long idObra;

    @Column(name = "id_versao_orcamento")
    private Long idVersaoOrcamento;

    public static String toLog(List<OrcamentoMentor> orcamentos){
        return orcamentos.stream().map(orcamento -> "idObra="+orcamento.getIdObra()+";versao="+orcamento.getIdVersaoOrcamento()+";codigo="+orcamento.getCodigo()+";\n").collect(Collectors.joining());
    }
}
