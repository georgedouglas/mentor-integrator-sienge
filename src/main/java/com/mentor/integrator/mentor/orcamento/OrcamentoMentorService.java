package com.mentor.integrator.mentor.orcamento;

import com.mentor.integrator.mentor.obra.ObraMentor;
import com.mentor.integrator.mentor.obra.ObraMentorRepository;
import com.mentor.integrator.sienge.planilha.PlanilhaSienge;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrcamentoMentorService { ;

    private final ObraMentorRepository obraMentorRepository;
    private final OrcamentoMentorRepository orcamentoMentorRepository;
    private final VersaoOrcamentoMentorRepository versaoOrcamentoMentorRepository;

    public List<ObraMentor> buscarTodasObrasSienge(){
        return obraMentorRepository.buscarTodasObrasSienge();
    }

    public boolean temOrcamento(Long idObra) {
        List<OrcamentoMentor> orcamentos = orcamentoMentorRepository.buscarOrcamentosPorObra(idObra);
        return orcamentos != null && !orcamentos.isEmpty();
    }

    public VersaoOrcamentoMentor recuperarVersaoOrcamento(Long idObra) {
        return versaoOrcamentoMentorRepository
            .buscarVersaoOrcamentoPorObra(idObra)
            .stream().max(Comparator.comparing(VersaoOrcamentoMentor::getId)).orElse(null);
    }

    public VersaoOrcamentoMentor migrarVersaoOrcamento(ObraMentor obraMentor) {
        VersaoOrcamentoMentor versaoOrcamento = VersaoOrcamentoMentor.builder()
                .idObra(obraMentor.getId())
                .data(OffsetDateTime.now())
                .versaoAtual(true)
                .descricao("IMPORTACAO SIENGE").build();
        return versaoOrcamentoMentorRepository.save(versaoOrcamento);
    }

    public List<OrcamentoMentor> atualizarVersaoOrcamento(ObraMentor obraMentor, VersaoOrcamentoMentor versaoOrcamentoMentor){
        List<OrcamentoMentor> orcamentos = orcamentoMentorRepository.buscarOrcamentosPorObra(obraMentor.getId());
        orcamentos.stream().forEach( u -> u.setIdVersaoOrcamento(versaoOrcamentoMentor.getId()));
        orcamentoMentorRepository.saveAll(orcamentos);
        return orcamentos;
    }

    public boolean deveMigrarOrcamento(PlanilhaSienge planilhaSienge, ObraMentor obraMentor) {
        List<OrcamentoMentor> orcamentos = orcamentoMentorRepository
                .buscarOrcamentosPorObraCodigo(obraMentor.getId(), planilhaSienge.getId());
        return orcamentos == null || orcamentos.isEmpty();
    }

    public OrcamentoMentor buscarOrcamentoPorObraCodigo(PlanilhaSienge planilhaSienge, ObraMentor obraMentor) {
        return orcamentoMentorRepository.buscarOrcamentoPorObraCodigo(obraMentor.getId(), planilhaSienge.getId());
    }

    public OrcamentoMentor migrarOrcamento(PlanilhaSienge planilhaSienge, ObraMentor obraMentor, VersaoOrcamentoMentor versaoOrcamentoMentor) {
        OrcamentoMentor orcamentoMentor = OrcamentoMentor.builder()
                .codigo(planilhaSienge.getId())
                .descricao(planilhaSienge.getDescription())
                .idObra(obraMentor.getId())
                .idVersaoOrcamento(versaoOrcamentoMentor.getId()).build();
        return orcamentoMentorRepository.save(orcamentoMentor);
    }
}
