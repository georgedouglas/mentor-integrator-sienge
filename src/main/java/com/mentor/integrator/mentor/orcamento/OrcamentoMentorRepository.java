package com.mentor.integrator.mentor.orcamento;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrcamentoMentorRepository extends CrudRepository<OrcamentoMentor, Long> {

    @Query(value = "SELECT o FROM OrcamentoMentor o WHERE o.idObra = :idObra")
    List<OrcamentoMentor> buscarOrcamentosPorObra(@Param("idObra") Long idObra);

    @Modifying
    @Query(value = "UPDATE OrcamentoMentor o set o.idVersaoOrcamento = :idVersaoOrcamento WHERE o.idObra = :idObra")
    void atualizarVersaoOrcamento(@Param("idVersaoOrcamento") Long idVersaoOrcamento, @Param("idObra") Long idObra);

    @Query(value = "SELECT o FROM OrcamentoMentor o WHERE o.idObra = :idObra and o.codigo = :codigo")
    List<OrcamentoMentor> buscarOrcamentosPorObraCodigo(@Param("idObra") Long idObra, @Param("codigo") Long codigo);

    @Query(value = "SELECT o FROM OrcamentoMentor o WHERE o.idObra = :idObra and o.codigo = :codigo")
    OrcamentoMentor buscarOrcamentoPorObraCodigo(@Param("idObra") Long idObra, @Param("codigo") Long codigo);
}
