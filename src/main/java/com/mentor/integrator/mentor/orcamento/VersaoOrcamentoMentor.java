package com.mentor.integrator.mentor.orcamento;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "versao_orcamento")
public class VersaoOrcamentoMentor {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_versao")
    private Long id;

    @Column(name = "data")
    private OffsetDateTime data;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "versao_atual")
    private boolean versaoAtual;

    @Column(name = "id_obra")
    private Long idObra;

    public static String toLog(List<VersaoOrcamentoMentor> versoes){
        return versoes.stream().map(versao -> "id="+versao.getId()+";idObra="+versao.getIdObra()+";\n").collect(Collectors.joining());
    }
}
