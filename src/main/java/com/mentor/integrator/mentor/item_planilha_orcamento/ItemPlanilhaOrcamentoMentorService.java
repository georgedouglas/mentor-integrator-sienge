package com.mentor.integrator.mentor.item_planilha_orcamento;

import com.mentor.integrator.mentor.orcamento.OrcamentoMentor;
import com.mentor.integrator.sienge.item_planilha.ItemPlanilhaSienge;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class ItemPlanilhaOrcamentoMentorService {

    private final ItemPlanilhaOrcamentoMentorRepository itemPlanilhaOrcamentoMentorRepository;

    public boolean deveMigrarItemPlanilha(ItemPlanilhaSienge itemPlanilhaSienge, OrcamentoMentor orcamentoMentor) {
        ItemPlanilhaOrcamentoMentor itemPlanilhaOrcamentoMentor = itemPlanilhaOrcamentoMentorRepository
                .buscarPorItemIntegracaoOrcamento(itemPlanilhaSienge.getId(), orcamentoMentor.getId());

        return itemPlanilhaOrcamentoMentor == null;
    }

    public ItemPlanilhaOrcamentoMentor migrarItemPlanilha(
            ItemPlanilhaSienge itemPlanilhaSienge,
            NivelItemPlanilha nivelItemPlanilha,
            OrcamentoMentor orcamentoMentor
        ){

        ItemPlanilhaOrcamentoMentor itemPlanilhaOrcamentoMentor = ItemPlanilhaOrcamentoMentor.builder()
                .descricao(itemPlanilhaSienge.getDescription())
                .referencia(itemPlanilhaSienge.getWbsCode())
                .idOrcamento(orcamentoMentor.getId())
                .idItemIntegracao(itemPlanilhaSienge.getId())
                .nivelItemPlanilha(nivelItemPlanilha).build();
        return itemPlanilhaOrcamentoMentorRepository.save(itemPlanilhaOrcamentoMentor);
    }

}
