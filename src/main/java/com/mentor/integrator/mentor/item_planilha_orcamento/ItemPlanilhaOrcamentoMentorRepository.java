package com.mentor.integrator.mentor.item_planilha_orcamento;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemPlanilhaOrcamentoMentorRepository extends CrudRepository<ItemPlanilhaOrcamentoMentor, Long> {

    @Query(value = "SELECT i FROM ItemPlanilhaOrcamentoMentor i WHERE i.idItemIntegracao = :idItemIntegracao and i.idOrcamento = :idOrcamento")
    ItemPlanilhaOrcamentoMentor buscarPorItemIntegracaoOrcamento(@Param("idItemIntegracao") Long idItemIntegracao, @Param("idOrcamento") Long idOrcamento);
}
