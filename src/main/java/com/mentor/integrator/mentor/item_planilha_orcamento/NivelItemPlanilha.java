package com.mentor.integrator.mentor.item_planilha_orcamento;

public enum NivelItemPlanilha {

    CELULA_CONSTRUTIVA(0, "Célula Construtiva"),
    ETAPA(1, "Etapa"),
    SUB_ETAPA(2, "Subetapa"),
    SERVICO(3, "Serviço");

    private int codigo;
    private String descricao;

    NivelItemPlanilha(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String toString(){
        return descricao;
    }

    public static NivelItemPlanilha valueOfCodigo(Integer codigo) {
        if (codigo == null) {
            return null;
        }
        for (NivelItemPlanilha nivelItemPlanilha : NivelItemPlanilha.values()) {
            if (nivelItemPlanilha.getCodigo() == codigo) {
                return nivelItemPlanilha;
            }
        }
        return null;
    }

    public static NivelItemPlanilha getNivel(String referencia){

        String[] classificacao;

        if(referencia.contains(".")){
            classificacao = referencia.replace(".",",").split(",");
        }else{
            classificacao = new String[1];
            classificacao[0] = referencia;
        }

        int nivel = classificacao.length;
        return NivelItemPlanilha.valueOfCodigo(nivel - 1);
    }
}
