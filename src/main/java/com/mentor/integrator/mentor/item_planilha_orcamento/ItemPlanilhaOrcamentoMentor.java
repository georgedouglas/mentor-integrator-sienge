package com.mentor.integrator.mentor.item_planilha_orcamento;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "item_planilha_orcamento")
public class ItemPlanilhaOrcamentoMentor {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_item_planilha_orcamento")
    private Long id;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "referencia")
    private String referencia;

    @Column(name="id_orcamento")
    private Long idOrcamento;

    @Column(name = "id_item_integracao")
    private Long idItemIntegracao;

    private NivelItemPlanilha nivelItemPlanilha;

    public static String toLog(List<ItemPlanilhaOrcamentoMentor> itens){
        return itens.stream().map(item -> "idOrcamento="+item.getIdOrcamento()+";idItemIntegracao="+item.getIdItemIntegracao()+";\n").collect(Collectors.joining());
    }
}
