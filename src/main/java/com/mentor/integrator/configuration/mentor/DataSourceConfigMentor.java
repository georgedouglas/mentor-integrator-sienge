package com.mentor.integrator.configuration.mentor;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
@ConfigurationProperties("spring.mentor")
public class DataSourceConfigMentor extends HikariConfigMentor {

    public DataSourceConfigMentor(HikariMentor hikariMentorProperties) {
        super(hikariMentorProperties);
    }

    @Bean
    public HikariDataSource dataSourceMentor() {
        return new HikariDataSource(this);
    }

    @Bean
    public JdbcTemplate jdbcTemplateMentor(@Qualifier("dataSourceMentor") DataSource ds) {
        return new JdbcTemplate(ds);
    }

    @Bean
    @DependsOn("dataSourceMentor")
    public NamedParameterJdbcTemplate namedJdbcTemplateMentor(@Qualifier("dataSourceMentor") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }
}
