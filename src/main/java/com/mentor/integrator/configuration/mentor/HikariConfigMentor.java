package com.mentor.integrator.configuration.mentor;

import com.zaxxer.hikari.HikariConfig;

import java.util.Properties;

public class HikariConfigMentor extends HikariConfig {

    protected final HikariMentor hikariMentorProperties;
    protected final String PERSISTENCE_UNIT_NAME = "mentor";
    protected final Properties JPA_MENTOR_PROPERTIES = new Properties() {{
        put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL10Dialect");
        put("show-sql", "true");
//        put("hibernate.hbm2ddl.auto", "update");
//        put("hibernate.ddl-auto", "update");
    }};

    protected HikariConfigMentor(HikariMentor hikariMentor) {
        this.hikariMentorProperties = hikariMentor;
        setPoolName(this.hikariMentorProperties.getPoolName());
        setMinimumIdle(this.hikariMentorProperties.getMinimumIdle());
        setMaximumPoolSize(this.hikariMentorProperties.getMaximumPoolSize());
        setIdleTimeout(this.hikariMentorProperties.getIdleTimeout());
    }
}

