package com.mentor.integrator.configuration.sienge;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Setter
@Getter
@Configuration
@PropertySource("classpath:application.yml")
@ConfigurationProperties("config.sienge")
public class SiengeConfig {

    private String baseUrl;
    private String subdominio;
    private String basePath;
    private String username;
    private String password;

    private int connectTimeOutInMillis;
    private long readTimeOutInMillis;
}
