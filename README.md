### Projeto de Integração da Sienge com o Mentor

#### Integrações utilizadas da Sienge - Orçamentos de Obra

- [Planilhas](https://api.sienge.com.br/docs/html-files/building-cost-estimations-v1.html#/Planilhas/get_building_cost_estimations__buildingId__sheets)
- [Itens da Planilha](https://api.sienge.com.br/docs/html-files/building-cost-estimations-v1.html#/Itens%20da%20Planilha/get_building_cost_estimations__building_id__sheets__building_unit_id__items)

#### Tabelas atualizadas do Mentor

- orcamento
- versao_orcamento
- item_planilha_orcamento
- servico_obra
- composicao_servico_informado
- unidade_medida
- grupo_insumo

#### DDL a ser executado uma vez. Necessário para startar o projeto

ALTER TABLE public.obra ADD COLUMN id_obra_integracao bigint;
ALTER TABLE public.item_planilha_orcamento ADD COLUMN id_item_integracao bigint;
